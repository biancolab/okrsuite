function gmb = OKRAnaly2(datadir)

if nargin<1
    if ismac
        datadir = uigetdir('/Users/ibianco/DATA/EXPT/OKR tests');
    else
        datadir = uigetdir('D:\2p_functional');
    end
end

h = waitbar(.5,'Loading gmb...');

load([datadir filesep 'gmb'])

minifilter = ones(1,5)./5;

if round(1/(gmb.p(1).timebase(2) - gmb.p(1).timebase(1)))~=100
    warndlg('Interpolation timbase is not 100 Hz')
    return
end

for p = 1:length(gmb.p)
    waitbar(p/length(gmb.p),h,'Processing...')
    gmb.p(p).Lvel = [];
    gmb.p(p).Rvel = [];
    gmb.p(p).stimvel = [];
    if ~isempty(gmb.p(p).selected)
        Lp = filtfilt(minifilter,1,gmb.p(p).Linterp);
        Rp = filtfilt(minifilter,1,gmb.p(p).Rinterp);
        Lv = [0 diff(Lp)].*100;
        Rv = [0 diff(Rp)].*100;
        for s = 1:size(gmb.p(p).selected, 1)
            t1 = findnearestx(gmb.p(p).selected(s,1), gmb.p(p).timebase, -1);
            t2 = findnearestx(gmb.p(p).selected(s,2), gmb.p(p).timebase, -1);
            
            svs = gmb.p(p).stimvelinterp(t1:t2);
            if any(isnan(svs))
                gmb.p(p).Lvel(s,1) = nan;
                gmb.p(p).Rvel(s,1) = nan;
                gmb.p(p).stimvel(s,1) = nan;
            else
                gmb.p(p).Lvel(s,1) = nanmean(Lv(t1:t2));
                gmb.p(p).Rvel(s,1) = nanmean(Rv(t1:t2));
                gmb.p(p).stimvel(s,1) = nanmean(svs);
            end
        end
    end
end


% vis characterised by sf, speed, contrast only
vis = gmb.visstim(:,3:5);
gmb.uniquevis = unique(vis,'rows');
gmb.gain = []; % gain, ignoring stim direction
gmb.gain_pos = []; % gain in pos dir
gmb.gain_neg = []; % gain in neg dir

for v = 1:size(gmb.uniquevis, 1)
    waitbar(v/size(gmb.uniquevis, 1),h,'Processing...')
    
    Lvel = [];
    Rvel = [];
    
    thisv = gmb.uniquevis(v, :);
    thesep = find(ismember(gmb.visstim(:, 3:5), thisv, 'rows'));
    
    Lvel = vertcat(gmb.p(thesep').Lvel);
    Rvel = vertcat(gmb.p(thesep').Rvel);
    stimvel = vertcat(gmb.p(thesep').stimvel);
    Lgain = Lvel./stimvel;
    Rgain = Rvel./stimvel;
    posvel = stimvel>0;
    
    gmb.v(v).vis = thisv;
    gmb.v(v).Lgain_posdir = Lgain(posvel);
    gmb.v(v).Lgain_negdir = Lgain(~posvel);
    gmb.v(v).Lgain = Lgain;
    gmb.v(v).Rgain_posdir = Rgain(posvel);
    gmb.v(v).Rgain_negdir = Rgain(~posvel);
    gmb.v(v).Rgain = Rgain;
    
    gmb.gain(v,1) = nanmean(gmb.v(v).Lgain);
    gmb.gain(v,2) = nanmean(gmb.v(v).Rgain);
    gmb.gain_pos(v,1) = nanmean(gmb.v(v).Lgain_posdir);
    gmb.gain_pos(v,2) = nanmean(gmb.v(v).Rgain_posdir);
    gmb.gain_neg(v,1) = nanmean(gmb.v(v).Lgain_negdir);
    gmb.gain_neg(v,2) = nanmean(gmb.v(v).Rgain_negdir);
end

% oculomotor range, saccade velocity
Lall = [gmb.p(:).Linterp];
gmb.Lrange = prctile(Lall, [2 50 98]);
Rall = [gmb.p(:).Rinterp];
gmb.Rrange = prctile(Rall, [2 50 98]);
Lp = filtfilt(minifilter,1,Lall);
Lv = [0 diff(Lp)].*100;
Rp = filtfilt(minifilter,1,Rall);
Rv = [0 diff(Rp)].*100;
gmb.LsaccVel = prctile(Lv, [0.5 50 99.5]);
gmb.RsaccVel = prctile(Rv, [0.5 50 99.5]);

waitbar(1,h,'Saving...');

save([datadir filesep 'gmb'],'gmb','-v7.3');

close(h);


%% Plotting

% SF
figure('Name', [gmb.name ' : OKR gain'], 'Position', [230 645 890 265])

subplot(1,3,1)
hold on
gv = unique(gmb.uniquevis(:,1)); % velocities
ct = unique(gmb.uniquevis(:,3));
SFall = [];
for g = 1:numel(gv)
    for c = 1:numel(ct)
        thesestim = ismember(gmb.uniquevis(:, [1 3]), [gv(g) ct(c)], 'rows');
        if any(thesestim)
            LRdata = gmb.gain(thesestim,1:2);
            SFrange = gmb.uniquevis(thesestim, 2);
            SFall = [SFall SFrange(:)'];
            SF.g(g).c(c).data = [SFrange LRdata];
            if numel(SFrange)>1 | size(gmb.visstim,1)==1
                plot(SFrange, LRdata(:,1), 'bo-')
                plot(SFrange, LRdata(:,2), 'ro-')
            end
        end   
    end
end
SFset = unique(SFall);
set(gca, 'TickDir', 'out', 'XTick', SFset)
ylabel('Eye Gain')
xlabel('Spatial Freq [cyc/360deg]')
title('Spatial Frequency')

% Vel
subplot(1,3,2)
hold on
sf = unique(gmb.uniquevis(:,2));
ct = unique(gmb.uniquevis(:,3));
Vall = [];
for s = 1:numel(sf)
    for c = 1:numel(ct)
        thesestim = ismember(gmb.uniquevis(:, [2 3]), [sf(s) ct(c)], 'rows');
        if any(thesestim)
            LRdata = gmb.gain(thesestim,1:2);
            Vrange = gmb.uniquevis(thesestim, 1);
            Vall = [Vall Vrange(:)'];
            V.s(s).c(c).data = [Vrange LRdata];
            if numel(Vrange)>1 | size(gmb.visstim,1)==1
                plot(Vrange, LRdata(:,1), 'bo-')
                plot(Vrange, LRdata(:,2), 'ro-')
            end
        end
    end
end
Vset = unique(Vall);
set(gca, 'TickDir', 'out', 'XTick', Vset)
ylabel('Eye Gain')
xlabel('Grating Velocity [deg/s]')
title('Grating Velocity')

% Contrast
subplot(1,3,3)
hold on
gv = unique(gmb.uniquevis(:,1));
sf = unique(gmb.uniquevis(:,2));
Call = [];
for g = 1:numel(gv)
    for s = 1:numel(sf)
        thesestim = ismember(gmb.uniquevis(:, [1 2]), [gv(g) sf(s)], 'rows');
        if any(thesestim)
            LRdata = gmb.gain(thesestim,1:2);
            Crange = gmb.uniquevis(thesestim, 3);
            Call = [Call Crange(:)'];
            C.g(g).s(s).data = [Crange LRdata];
            if numel(Crange)>1 | size(gmb.visstim,1)==1
                plot(Crange, LRdata(:,1), 'bo-')
                plot(Crange, LRdata(:,2), 'ro-')
            end
        end
    end
end
Cset = unique(Call);
set(gca, 'TickDir', 'out', 'XTick', Cset)
ylabel('Eye Gain')
xlabel('Contrast [a.u.]')
title('Contrast')

figure('Name', [gmb.name ' : Oculomotor range and saccadic eye velocity'], 'Position', [1125 645 465 265])
subplot(1,2,1)
plot([1 1],[gmb.Lrange(1) gmb.Lrange(3)], 'b-o')
hold on
plot([1],[gmb.Lrange(2)], 'bx')
plot([2 2],[gmb.Rrange(1) gmb.Rrange(3)], 'r-o')
plot([2],[gmb.Rrange(2)], 'rx')
xlim([0 3])
set(gca, 'XTick', [1 2], 'XTickLabel', {'Left' 'Right'})
ylabel('Position [deg]')
title('Position [2 50 98]')
subplot(1,2,2)
plot([1 1],[gmb.LsaccVel(1) gmb.LsaccVel(3)], 'b-o')
hold on
plot([1],[gmb.LsaccVel(2)], 'bx')
plot([2 2],[gmb.RsaccVel(1) gmb.RsaccVel(3)], 'r-o')
plot([2],[gmb.RsaccVel(2)], 'rx')
xlim([0 3])
set(gca, 'XTick', [1 2], 'XTickLabel', {'Left' 'Right'})
ylabel('Velocity [deg/s]')
title('Velocity [1 50 99]')



function [r,c,V] = findnearestx(srchvalue,srcharray,bias)
        
        % Usage:
        % Find the nearest numerical value in an array to a search value
        % All occurances are returned as array subscripts
        %
        % Output:
        %
        % For 2D matrix subscripts (r,c) use:
        %
        %       [r,c] = findnearest(srchvalue,srcharray,gt_or_lt)
        %
        %
        % To also output the found value (V) use:
        %
        %       [r,c,V] = findnearest(srchvalue,srcharray,gt_or_lt)
        %
        %
        % For single subscript (i) use:
        %
        %         i   = findnearest(srchvalue,srcharray,gt_or_lt)
        %
        %
        % Inputs:
        %
        %    srchvalue = a numerical search value
        %    srcharray = the array to be searched
        %    bias      = 0 (default) for no bias
        %                -1 to bias the output to lower values
        %                 1 to bias the search to higher values
        %                (in the latter cases if no values are found
        %                 an empty array is ouput)
        %
        %
        % By Tom Benson (2002)
        % University College London
        % t.benson@ucl.ac.uk
        
        if nargin<2
            error('Need two inputs: Search value and search array')
        elseif nargin<3
            bias = 0;
        end
        
        % find the differences
        srcharray = srcharray-srchvalue;
        
        if bias == -1   % only choose values <= to the search value
            
            srcharray(srcharray>0) =inf;
            
        elseif bias == 1  % only choose values >= to the search value
            
            srcharray(srcharray<0) =inf;
            
        end
        
        % give the correct output
        if nargout==1 | nargout==0
            
            if all(isinf(srcharray(:)))
                r = [];
            else
                r = find(abs(srcharray)==min(abs(srcharray(:))));
            end
            
        elseif nargout>1
            if all(isinf(srcharray(:)))
                r = [];c=[];
            else
                [r,c] = find(abs(srcharray)==min(abs(srcharray(:))));
            end
            
            if nargout==3
                V = srcharray(r,c)+srchvalue;
            end
        end
