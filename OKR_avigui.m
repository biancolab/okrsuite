function varargout = OKR_avigui(varargin)
% OKR_AVIGUI MATLAB code for OKR_avigui.fig
%      OKR_AVIGUI, by itself, creates a new OKR_AVIGUI or raises the existing
%      singleton*.
%
%      H = OKR_AVIGUI returns the handle to a new OKR_AVIGUI or the handle to
%      the existing singleton*.
%
%      OKR_AVIGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OKR_AVIGUI.M with the given input arguments.
%
%      OKR_AVIGUI('Property','Value',...) creates a new OKR_AVIGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OKR_avigui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OKR_avigui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OKR_avigui

% Last Modified by GUIDE v2.5 14-Dec-2016 11:03:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OKR_avigui_OpeningFcn, ...
                   'gui_OutputFcn',  @OKR_avigui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OKR_avigui is made visible.
function OKR_avigui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OKR_avigui (see VARARGIN)

% Choose default command line output for OKR_avigui
handles.output = hObject;

imshow(zeros(300), 'Parent',handles.axes1);
axis off


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OKR_avigui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OKR_avigui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in select_data.
function select_data_Callback(hObject, eventdata, handles)
% hObject    handle to select_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get Movie

[fn, pn] = uigetfile('*.*','Select the movie to analyse...');
fname = fullfile(pn, fn);

% fname = '/Users/ibianco/Desktop/winkler/4dpf_wt_F2_5 x _0 speed_black_2.avi';
% fname = '/Users/ibianco/Desktop/winkler/media8.mov';
% fname = '/Users/ibianco/Desktop/winkler/wink_jpeg_1fps_reg.avi';
% fname = '/Users/ibianco/Desktop/winkler/4dpf_5x_3_5fps_cropreg.avi';
% pn = '/Users/ibianco/Desktop/winkler';

handles.pn = pn;

% Get visdir file

fvname = fullfile(pn, 'visdir.txt');
% fvname = '/Users/ibianco/Desktop/winkler/visdir.txt';

handles.visdir = dlmread(fvname);

v = VideoReader(fname);
handles.numframes = v.NumberOfFrames;
disp(['Movie has ' num2str(handles.numframes) ' frames...'])

handles.v = VideoReader(fname);
handles.v.CurrentTime = round(handles.v.Duration./2);

handles.fmiddle = readFrame(handles.v);

imshow(handles.fmiddle, 'Parent',handles.axes1);
set(handles.message_box, 'String', 'Select Head');

h = imrect(handles.axes1);
handles.croprect = round(h.getPosition);

fmiddle_crop = handles.fmiddle(handles.croprect(2):handles.croprect(2)+handles.croprect(4), handles.croprect(1):handles.croprect(1)+handles.croprect(3), :);
handles.fmiddle_crop = mean(fmiddle_crop, 3)./255;

imshow(handles.fmiddle_crop, 'Parent',handles.axes1)
set(handles.message_box, 'String', 'Select L eye');
h = impoint(handles.axes1);
handles.Lcntr = h.getPosition;
set(handles.message_box, 'String', 'Select R eye');
h = impoint(handles.axes1);
handles.Rcntr = h.getPosition;
set(handles.message_box, 'String', 'Select Body Axis');
h = imline(handles.axes1);
Bline = round(h.getPosition);
Bangl = okranglcalc(Bline(1,1),Bline(1,2),Bline(2,1),Bline(2,2));
if Bangl > 180
    Bangl = Bangl - 180;
end
handles.Bangl = Bangl;

set(handles.message_box, 'String', 'Adjust Threshold, enter trial ID and then press RUN');
guidata(hObject, handles);


% --- Executes on slider movement.
function threshold_slider_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.th = get(hObject,'Value');
handles.dmode = 'threshold';
guidata(hObject, handles);
updatedisplay(handles);

% --- Executes during object creation, after setting all properties.
function threshold_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to threshold_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function updatedisplay(handles)
% display stuff
if strcmp(handles.dmode, 'threshold')
    thimj = handles.fmiddle_crop;
    thimj = im2bw(thimj, handles.th);
    thimj = ~thimj;
    imshow(thimj, 'Parent',handles.axes1);
    text(10, 20, num2str(handles.th), 'Color', 'r', 'FontSize', 21)
elseif strcmp(handles.dmode, 'live')
%     thimj = im2bw(handles.thisimj, handles.th);
    thimj = handles.thisimj;
%     thimj = ~thimj;
    imshow(thimj, 'Parent',handles.axes1);
end


% --- Executes on button press in run_button.
function run_button_Callback(hObject, eventdata, handles)
% hObject    handle to run_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = [];
handles.v.CurrentTime = 0;
handles.dmode = 'live';
k = 1;
w = waitbar(0,'Processing Movie...');

while hasFrame(handles.v)
    waitbar(k/handles.numframes, w, 'Processing Movie...')
    ti = readFrame(handles.v);
    thisimj = ti( handles.croprect(2):handles.croprect(2)+handles.croprect(4), handles.croprect(1):handles.croprect(1)+handles.croprect(3), : );
    thisimj = mean(thisimj, 3)./255;
    
    BW = ~im2bw(thisimj, handles.th);
    BW = imfill(BW, 'holes');
    L = bwlabel(BW);
    R = regionprops(BW, 'Area', 'Centroid', 'Orientation');
    allArea = cat(1, R(:).Area);
    allCnt = cat(1, R(:).Centroid);
    allOri = cat(1, R(:).Orientation);
    [~, ixx] = sort(cat(1, R(:).Area), 'descend');
    eyeix = ixx(1:2);
    eyeArea = allArea(eyeix);
    eyeCnt = allCnt(eyeix, :);
    eyeOri = allOri(eyeix);
    
    % sort L and R
    [~, emix] = min(sum((repmat(handles.Lcntr, [2 1]) - eyeCnt).^2, 2));
    if emix~=1
        eyeArea = eyeArea([2 1]);
        eyeCnt = eyeCnt([2 1],:);
        eyeOri = eyeOri([2 1]);
    end
    % Langl
    La = eyeOri(1);
    if La<0
        La = La + 180;
    end
    Langl = handles.Bangl - La;
    
    % Rangl
    Ra = eyeOri(2);
    if Ra<0
        Ra = Ra + 180;
    end
    Rangl = handles.Bangl - Ra;

    data(k, :) = [handles.v.CurrentTime, Langl, Rangl, k, handles.p, nan, 0];

    handles.thisimj = BW;
    guidata(hObject, handles);
    updatedisplay(handles);
    k = k + 1;
end
% insert visdir information
waitbar(0.5, w, 'Processing visdir...')
for v = 1:size(handles.visdir, 1)
    stt = findnearestx(handles.visdir(v,1), data(:,1), -1);
    edd = findnearestx(handles.visdir(v,2), data(:,1), -1);
    data(stt:edd, 6) = handles.visdir(v,3);
end

% change time to ms
data(:,1) = data(:,1).*1000;

assignin('base', 'data', data);

waitbar(0.5, w, 'Saving data...')
datars = data';
dlmwrite([fullfile(handles.pn, num2str(handles.p)) '.okr'], datars(:)' , '\t');

pause(1)
close(w);
disp('... Done.')

figure('Name', 'Eye position. L (blue) R (red)')
plot(data(:,1)./1000, data(:,2), 'b'), hold on, plot(data(:,1)./1000, data(:,3), 'r')
plot(data(:,1)./1000, data(:,6), 'k')
xlabel('Time [s]'), ylabel('Eye Position [deg]')


function trialID_box_Callback(hObject, eventdata, handles)
% hObject    handle to trialID_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trialID_box as text
%        str2double(get(hObject,'String')) returns contents of trialID_box as a double
handles.p = str2double(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function trialID_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trialID_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function anglecorr=okranglcalc(x1,y1,x2,y2)

%calculates angle from line 1, counterclockwise to line 2, where line 1 is
%the right horizontal from point x1,y1 and line 2 is the line connecting
%x1,y1 with x2,y2


anglraw=(180/pi)*atan(abs(y2-y1)/abs(x2-x1));


if y2<=y1 && x2<x1
    anglecorr=180-anglraw;
elseif y2>y1 && x2<=x1
    anglecorr=180+anglraw;
elseif y2>y1 && x2>x1
    anglecorr=360-anglraw;
elseif y2==y1 && x2==x1
    anglecorr=0;
else
    anglecorr=anglraw;
end;


function [r,c,V] = findnearestx(srchvalue,srcharray,bias)

% Usage:
% Find the nearest numerical value in an array to a search value
% All occurances are returned as array subscripts
%
% Output:
%
% For 2D matrix subscripts (r,c) use:
%
%       [r,c] = findnearest(srchvalue,srcharray,gt_or_lt)
%
%
% To also output the found value (V) use:
%
%       [r,c,V] = findnearest(srchvalue,srcharray,gt_or_lt)
%
%
% For single subscript (i) use:
%
%         i   = findnearest(srchvalue,srcharray,gt_or_lt)
% 
%
% Inputs:
%
%    srchvalue = a numerical search value
%    srcharray = the array to be searched
%    bias      = 0 (default) for no bias
%                -1 to bias the output to lower values
%                 1 to bias the search to higher values
%                (in the latter cases if no values are found
%                 an empty array is ouput)
%
%
% By Tom Benson (2002)
% University College London
% t.benson@ucl.ac.uk

if nargin<2
    error('Need two inputs: Search value and search array')
elseif nargin<3
    bias = 0;
end

% find the differences
srcharray = srcharray-srchvalue;

if bias == -1   % only choose values <= to the search value
    
    srcharray(srcharray>0) =inf;
        
elseif bias == 1  % only choose values >= to the search value
    
    srcharray(srcharray<0) =inf;
        
end

% give the correct output
if nargout==1 | nargout==0
    
    if all(isinf(srcharray(:)))
        r = [];
    else
        r = find(abs(srcharray)==min(abs(srcharray(:))));
    end 
        
elseif nargout>1
    if all(isinf(srcharray(:)))
        r = [];c=[];
    else
        [r,c] = find(abs(srcharray)==min(abs(srcharray(:))));
    end
    
    if nargout==3
        V = srcharray(r,c)+srchvalue;
    end
end


    

