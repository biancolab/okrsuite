# README #

### What is this repository for? ###

* This is a suite of MATLAB functions for analysis of OKR data acquired with Bianco Lab OKR aquisition software (v2.00 or later). It also includes software for analysis of OKR movies produced with third party software.

### How do I get set up? ###

* Download the functions (Navigation>Downloads) and place in MATLAB path.

* See the included documentation `OKR_Analysis_Guide`.

* Note that the GUIs provide a convenient means to analyse data. However, individual functions for data extraction, segment selection and analysis can be run individually, if preferred.

### Problems ###

Please raise an Issue (Navigation>Issues) if you encounter bugs.