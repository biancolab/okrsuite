function varargout = OKRmulti(varargin)
% OKRMULTI MATLAB code for OKRmulti.fig
%      OKRMULTI, by itself, creates a new OKRMULTI or raises the existing
%      singleton*.
%
%      H = OKRMULTI returns the handle to a new OKRMULTI or the handle to
%      the existing singleton*.
%
%      OKRMULTI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OKRMULTI.M with the given input arguments.
%
%      OKRMULTI('Property','Value',...) creates a new OKRMULTI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OKRmulti_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OKRmulti_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OKRmulti

% Last Modified by GUIDE v2.5 11-Jun-2016 14:17:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OKRmulti_OpeningFcn, ...
                   'gui_OutputFcn',  @OKRmulti_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OKRmulti is made visible.
function OKRmulti_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OKRmulti (see VARARGIN)

% set up some basics
if ismac
    dirspp='/';
else
    dirspp='\';
end;
handles.dirspp = dirspp;
handles.dispmode = 1;
handles.velthr = 50; % default 50 deg/s

% Choose default command line output for OKRmulti
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OKRmulti wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OKRmulti_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in selectdata.
function selectdata_Callback(hObject, eventdata, handles)
% hObject    handle to selectdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


if isfield(handles, 'gmb')
    choice = questdlg('Quit this project and load new?','Load New Project','Yes', 'No', 'No');
    if strcmp(choice, 'No')
        return
    end
end

datadir = uigetdir('/Users/ibianco/DATA/EXPT/OKR tests');

if exist([datadir filesep 'gmb.mat'])>0
    choice = questdlg('Reset this project?','Reset Project','Reset', 'Keep existing', 'Keep existing');
    if strcmp(choice, 'Keep existing')
        load([datadir handles.dirspp 'gmb'])
    else
        gmb = OKRExtr2(datadir);
    end
else
    gmb = OKRExtr2(datadir);
end

trial_list = {};
for p = 1:length(gmb.p)
    trial_list{p} = ['Trial ' num2str(p)];
end
trialz = [1:length(gmb.p)];

set(handles.textbox,'String',gmb.name);
set(handles.trialmenu,'String',trial_list,'Value',1);

handles.datadir = datadir;
handles.gmb = gmb;
handles.trialz = trialz;
handles.thistrial = 1;

guidata(hObject, handles);
plotokrdata(handles)



% --- Executes on selection change in trialmenu.
function trialmenu_Callback(hObject, eventdata, handles)
% hObject    handle to trialmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns trialmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from trialmenu

thistrial = get(hObject,'Value');
handles.thistrial = thistrial;

guidata(hObject, handles);
plotokrdata(handles)


% --- Executes during object creation, after setting all properties.
function trialmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trialmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in mark.
function mark_Callback(hObject, eventdata, handles)
% hObject    handle to mark (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.textbox,'String','Select Start');
st = impoint(handles.axes1,[]);
set(handles.textbox,'String','Select End');
ed = impoint(handles.axes1,[]);
st = getPosition(st);
ed = getPosition(ed);

handles.gmb.p(handles.thistrial).selected = [handles.gmb.p(handles.thistrial).selected; [st(1) ed(1)] ];

set(handles.textbox,'String','Marked')

assignin('base', 'gmb', handles.gmb)

savedata(handles)

guidata(hObject, handles);


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

choice = questdlg('Clear all segments for this trial?','Clear all','Yes', 'No', 'No');
if strcmp(choice, 'No')
	return
end

handles.gmb.p(handles.thistrial).selected = [];
set(handles.textbox,'String','Cleared')

assignin('base', 'gmb', handles.gmb)

savedata(handles)

guidata(hObject, handles);



function plotokrdata(handles)

L = handles.gmb.p(handles.thistrial).Linterp;
R = handles.gmb.p(handles.thistrial).Rinterp;
tb = handles.gmb.p(handles.thistrial).timebase;
stimvel = handles.gmb.p(handles.thistrial).stimvelinterp;
sel = handles.gmb.p(handles.thistrial).selected;
minifilter = ones(1,5)./5;

if handles.dispmode==2
    L = filtfilt(minifilter,1,L);
    R = filtfilt(minifilter,1,R);
    L = [0 diff(L)].*100;
    R = [0 diff(R)].*100;
end

plot(0,0,'.')
hold on
for s = 1:size(sel,1)
%     line([sel(s,1) sel(s,1)],[-20 20],'Color','c')
    fill([sel(s,1) sel(s,1) sel(s,2) sel(s,2)], [-20 20 20 -20], [.92 .92 .92])
%     line([sel(s,2) sel(s,2)],[-20 20],'Color','k')
end

plot(handles.axes1,tb, stimvel, '--', 'Color', [.5 .5 .5], 'LineWidth', 4)
plot(handles.axes1,tb, L, 'b-', 'LineWidth', 1.5);
plot(handles.axes1,tb, R, 'r-', 'LineWidth', 1.5);
hx = xlabel('Time [s]');
set(hx, 'FontSize', 15);
if handles.dispmode==2
    h = ylabel('Eye Velocity [deg/s]');
    set(h, 'FontSize', 15) 
else
    h = ylabel('Eye Position [deg]');
    set(h, 'FontSize', 15)
end

set(handles.axes1,'NextPlot','replace');
trinfostr = ['   spfreq: ' num2str(handles.gmb.visstim(handles.thistrial,4)) '   velocity: ' num2str(handles.gmb.visstim(handles.thistrial,3)) '   contrast: ' num2str(handles.gmb.visstim(handles.thistrial,5))];
if size(handles.gmb.visstim,2)==7
    trinfostr = [trinfostr '   Eye: ' num2str(handles.gmb.visstim(handles.thistrial,7))];
end
dispstr = sprintf('%s\n%s',handles.gmb.name, ['Trial: ' num2str(handles.thistrial) trinfostr]);
set(handles.textbox,'String',dispstr,'FontSize',12);


function savedata(handles)

gmb = handles.gmb;
save([handles.datadir handles.dirspp 'gmb'],'gmb','-v7.3');


% --- Executes on button press in analyze.
function analyze_Callback(hObject, eventdata, handles)
% hObject    handle to analyze (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.gmb = OKRAnaly2(handles.datadir);

assignin('base', 'gmb', handles.gmb)

% no need to save - OKRAnaly does this


% --- Executes on button press in velocity_button.
function velocity_button_Callback(hObject, eventdata, handles)
% hObject    handle to velocity_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of velocity_button

if get(hObject,'Value');
    handles.dispmode = 2;
else
    handles.dispmode = 1;
end

guidata(hObject, handles);
plotokrdata(handles)


% --- Executes on button press in autosaccadeseg.
function autosaccadeseg_Callback(hObject, eventdata, handles)
% hObject    handle to autosaccadeseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

choice = questdlg('Automatic segments for which trials?','How lucky?','All trials', 'This trial', 'Cancel', 'Cancel');
switch choice
    case 'Cancel'
        return
    case 'This trial'
        handles.gmb = OKR_saccadeseg(handles.datadir, handles.velthr, handles.thistrial);
        assignin('base', 'gmb', handles.gmb)
        % no need to save - OKRAnaly does this
    case 'All trials'
        handles.gmb = OKR_saccadeseg(handles.datadir, handles.velthr);
        assignin('base', 'gmb', handles.gmb)
        % no need to save - OKRAnaly does this
    otherwise
        return
end

guidata(hObject, handles);
plotokrdata(handles)



function Vel_thr_box_Callback(hObject, eventdata, handles)
% hObject    handle to Vel_thr_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Vel_thr_box as text
%        str2double(get(hObject,'String')) returns contents of Vel_thr_box as a double
handles.velthr = str2double(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function Vel_thr_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vel_thr_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
