function gmb = OKR_saccadeseg(datadir, vthr, prange)

if nargin<1
    if ismac
        datadir = uigetdir('/Users/ibianco/DATA/EXPT/OKR tests');
    else
        datadir = uigetdir('D:\2p_functional');
    end
end

h = waitbar(.5,'Loading gmb...');

load([datadir filesep 'gmb'])

if nargin<3
    prange = [1:length(gmb.p)];
end
if nargin<2
    vthr = 20;
end

minifilter = ones(1,5)./5; % 50 ms boxcar

fps = 1./mode(diff(gmb.p(1).tt)); % estimate frame rate

% tune parameteres based on sampling rate
if fps < 5
    seglnth = 200;      % 2s
    saccspread = 100;   % 1 s
    seg_min = 0.75;     % segment minimum duration [s]
elseif fps < 20
    seglnth = 100;      % 1s
    saccspread = 30;    % 300 ms
    seg_min = 0.5;      % segment minimum duration [s]
else
    seglnth = 100;      % standard slow phase segments [its]: 1 s
    saccspread = 10;    % exclusion zone around saccade [its]: 100ms pre/post saccade
    seg_min = 0.3;      % segment minimum duration [s] was 0.3
end

if round(1/(gmb.p(1).timebase(2) - gmb.p(1).timebase(1)))~=100
    warndlg('Interpolation timbase is not 100 Hz')
    return
end

%%
for p = prange
    waitbar(p/length(gmb.p),h,'Processing...')
    
    gmb.p(p).vthr = vthr; % keep record of vthr for this p
    
    Lp = filtfilt(minifilter,1,gmb.p(p).Linterp);
    Rp = filtfilt(minifilter,1,gmb.p(p).Rinterp);
    Lv = [0 diff(Lp)].*100;
    Rv = [0 diff(Rp)].*100;
    
    Lsacc = abs(Lv)>vthr;
    Rsacc = abs(Rv)>vthr;
    
    Sv = gmb.p(p).stimvelinterp;
    Sv(isnan(Sv)) = 0;
    % all changes in stim velocity: ignore 1 sec after
    Svd = abs(sign([0 diff(Sv)]));
    Svdig = zeros(1,numel(Svd));
    for s = 1:numel(Svd)
        if Svd(s)==1
            Svdig(s:s+100) = 1;
        end
    end
    Svdig = Svdig(1:numel(Svd)); %constrain size of Svd
    
    ign = Lsacc | Rsacc;
    igHI = find([0 diff(ign)] == 1);
    igLO = find([0 diff(ign)] == -1);
    
    gmb.p(p).num_sacc = [sum([0 diff(Lsacc)] == 1), sum([0 diff(Rsacc)] == 1)];
    
    for i = 1:numel(igHI)
        if igHI(i)>=saccspread
            ign(igHI(i)-saccspread+1:igHI(i)) = 1;
        else
            ign(1:igHI(i)) = 1;
        end
    end
    for i = 1:numel(igLO)
        if igLO(i) <= numel(ign)-saccspread+1
            ign(igLO(i):igLO(i)+saccspread-1) = 1;
        else
            ign(igLO(i):end) = 1;
        end
    end
    % and ignore first 200 ms of recording
    ign(1:20) = 1;
    ign = ign + Svdig;
    
    igHI = find([0 diff(ign)] == 1);
    igLO = find([0 diff(ign)] == -1);
    
    seg = [];
    lastend = 1;
    for i = 1:numel(ign)
        if i>lastend && ign(i)==0 && i<(numel(ign)-seglnth) && all(ign(i:i+seglnth)==0)
            seg = [seg; i i+seglnth];
            lastend = i+seglnth;
        elseif i>lastend && ign(i)==0 && i<(numel(ign)-5)
            igg = igHI;
            igg(igg<=i) = [];
            if ~isempty(igg)
                seg = [seg; i min(igg)];
                lastend = min(igg);
            end
        end
    end
    
    seg_t = [];
    seg_t = gmb.p(p).timebase(seg);
    
    % remove small segments
    try
        seg_dur = seg_t(:,2) - seg_t(:,1);
        killix = seg_dur < seg_min;
        seg_t(killix,:) = [];
    end
    
    gmb.p(p).selected = seg_t;
    
end


%%
waitbar(1,h,'Saving...')

save([datadir filesep 'gmb'],'gmb','-v7.3');

close(h);




